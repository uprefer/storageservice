# StorageService

[![pipeline status](https://gitlab.com/uprefer/storageservice/badges/master/pipeline.svg)](https://gitlab.com/uprefer/storageservice/commits/master)
[![coverage report](https://gitlab.com/uprefer/storageservice/badges/master/coverage.svg)](https://gitlab.com/uprefer/storageservice/commits/master)

## What's that ?!

**StorageService** is a simple HTTP API that stores files.

It relies on **MongoDB** to store files and theirs metadata. 


## Production deployment

We recommand usage of Docker.

### Docker Compose

You can use [this](docker-compose.yml)  minimal `docker-compose.yml`  to run **StorageService**.

```bash
$ docker-compose up
```

### Docker

```bash
$ docker run \
    -v db:/data/db\
    -e STORAGESERVICE_MODE=release\
    -e STORAGESERVICE_MONGO_IP=mongo\
    -e STORAGESERVICE_MONGO_PORT=27017\
    -e STORAGESERVICE_MONGO_USER=StorageService\
    -e STORAGESERVICE_MONGO_PASSWORD=StorageService\
    -e STORAGESERVICE_MONGO_DB=StorageService\
    -e STORAGESERVICE_PORT=80\
    registry.gitlab.com/uprefer/storageservice:latest
```

## Development

To run a development instance and use debug, we recommand you to run dependencies via **Docker Compose**, and debug **StorageService** from your IDE.

###  Run dependencies

You can use [this](docker-compose-dev.yml)  minimal `docker-compose.yml`  to run **MongoDB** and **MongoExpress**.

```bash
$ docker-compose -f dev_files/docker-compose-dev.yml up
```

### Run StorageService

Setup your IDE to run `src/app.go` using the environment variables file [dev_files/api_dev.env](dev_files/api_dev.env)

## Usage

[Insomnia](https://insomnia.rest/) configuration file [is available in repository](insomnia_conf.json).
