FROM golang:1.13
MAINTAINER Charly Caulet <contact@charly-caulet.net>

WORKDIR /app
COPY ./src .
RUN go get
RUN go build -o storageservice

CMD ["./storageservice"]