package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/uprefer/storageservice/config"
	"gitlab.com/uprefer/storageservice/controller"
	"gitlab.com/uprefer/storageservice/dao"
	"gitlab.com/uprefer/storageservice/database"
	"gitlab.com/uprefer/storageservice/service"
)

func Handlers(config *config.Configuration) *gin.Engine {
	ginMode := gin.ReleaseMode
	if config.IsDebugging() {
		ginMode = gin.DebugMode
	}
	gin.SetMode(ginMode)

	engine := gin.Default()
	engine.Use(cors.Default())

	var (
		db = database.NewDatabase("mongodb://"+config.MongoUser+":"+config.MongoPassword+"@"+config.MongoIp+":"+config.MongoPort, config.MongoDB)

		artifactDao = dao.NewArtifactDao(db)
		blobDao     = dao.NewMongoBlobDao(db)

		utilsService    = service.NewUtilsService()
		artifactService = service.NewArtifactService(utilsService, artifactDao)
		blobService     = service.NewBlobService(blobDao, artifactDao)

		artifactController = controller.NewArtifactController(artifactService)
		blobController     = controller.NewBlobController(blobService)
	)

	v1 := engine.Group("/v1")
	{
		artifactGroup := v1.Group("/artifact")
		{
			artifactGroup.POST("", artifactController.Post)
		}

		artifactResource := artifactGroup.Group(":artifact_id")
		{
			artifactResource.GET("", artifactController.Get)
		}

		blobResource := artifactResource.Group("/blob")
		{
			blobResource.PUT("", blobController.Put)
			blobResource.GET("", blobController.Get)
		}
	}
	return engine
}
